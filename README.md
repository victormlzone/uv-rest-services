### Setting up Local Development
https://cloud.google.com/nodejs/docs/setup

Steps:
- Install Node Version Manager
- Install Node.js
- Install NPM (Node Package Manager)
- Install Node.js Editor (Visual Studio Code - Prefered)
- Install Google Cloud SDK
- Install PostMan (to test your REST API, either local or remote)

### Validate Node.JS Installation (from command line)

node -v

### Setup Google Cloud Local Env (from CLI)

gcloud components update
gcloud auth application-default login
gcloud init

### Once your REST API is defined with Swagger Editor 2.0, Generate Server code for Node.js, open the directory in your Visual Studio Code.


### To install all NPM dependencies from package.json (either from Command line or Terminal in Visual Studio Code)

nmp install

### To run App in your local env:

npm start

### To see Swagger UI of your REST API

http://localhost:8080/docs

### To stop the App -> Ctrl + C (and say Yes)

### To deploy App to GAE you need to create your app.yaml file. Example:

service: servicios-uv
runtime: nodejs
env: flex

resources:
  cpu: 2
  memory_gb: 2.3
  disk_size_gb: 10

manual_scaling:
  instances: 1

### The execute this command to deploy the app to Google Cloud:

npm run deploy

### In case you want to use Cloud Endpoints to manage (add security, documentation, etc) your REST API, run:

gcloud endpoints services deploy api/swagger.yaml

### And add following lines in your app.yaml to bind your Backend API REST with Cloud Endpoints layer:

endpoints_api_service:
  name: "weighty-casing-219915.appspot.com"
  rollout_strategy: managed

### Re-deploy your app everytime you add new functionality
'use strict';


/**
 * Crea un nuevo alumno.
 * Crea un nuevo alumno.
 *
 * alumno Alumno El objeto Alumno a guardar (optional)
 * no response value expected for this operation
 **/
exports.addAlumno = function(alumno) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Actualiza alumno.
 * Actualiza los datos de un alumno.
 *
 * alumno Alumno El objeto Alumno a actualiza (optional)
 * no response value expected for this operation
 **/
exports.editAlumno = function(alumno) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Obtiene un alumno por su numero de matricula.
 * Regresa el alumno por Id.
 *
 * matricula String Matricula del alumno
 * returns Alumno
 **/
exports.getAlumno = function(matricula) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "apaterno" : "apaterno",
  "foto" : "foto",
  "matricula" : "matricula",
  "amaterno" : "amaterno",
  "semestre" : 0,
  "sexo" : "M",
  "nombre" : "nombre"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Obtiene la lista de alumnos.
 * Regresa una lista de alumnos.
 *
 * returns List
 **/
exports.getAlumnos = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "apaterno" : "apaterno",
  "foto" : "foto",
  "matricula" : "matricula",
  "amaterno" : "amaterno",
  "semestre" : 0,
  "sexo" : "M",
  "nombre" : "nombre"
}, {
  "apaterno" : "apaterno",
  "foto" : "foto",
  "matricula" : "matricula",
  "amaterno" : "amaterno",
  "semestre" : 0,
  "sexo" : "M",
  "nombre" : "nombre"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


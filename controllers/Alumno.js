'use strict';

var utils = require('../utils/writer.js');
var Alumno = require('../service/AlumnoService');

module.exports.addAlumno = function addAlumno (req, res, next) {
  var alumno = req.swagger.params['alumno'].value;
  Alumno.addAlumno(alumno)
    .then(function (response) {    
      utils.writeJson(res, response, 201);
    })
    .catch(function (response) {
      utils.writeJson(res, response, 405);
    });
};

module.exports.editAlumno = function editAlumno (req, res, next) {
  var alumno = req.swagger.params['alumno'].value;
  Alumno.editAlumno(alumno)
    .then(function (response) {
      utils.writeJson(res, response, 201);
    })
    .catch(function (response) {
      utils.writeJson(res, response, 405);
    });
};

module.exports.getAlumno = function getAlumno (req, res, next) {
  var matricula = req.swagger.params['matricula'].value;
  Alumno.getAlumno(matricula)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAlumnos = function getAlumnos (req, res, next) {
  Alumno.getAlumnos()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

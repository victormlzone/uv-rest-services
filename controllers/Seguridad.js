'use strict';

var utils = require('../utils/writer.js');
var Seguridad = require('../service/SeguridadService');

module.exports.finalizarSesion = function finalizarSesion (req, res, next) {
  Seguridad.finalizarSesion()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.iniciarSesion = function iniciarSesion (req, res, next) {
  var usuario = req.swagger.params['usuario'].value;
  Seguridad.iniciarSesion(usuario)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
